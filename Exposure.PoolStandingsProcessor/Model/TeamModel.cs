﻿using Exposure.Common;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exposure.PoolStandingsProcessor.Model
{
    public class TeamModel 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<GameModel> Games { get; set; } = new List<GameModel>();
        // Contains Exhibition
        public List<GameModel> AllGames { get; set; } = new List<GameModel>();

        public List<TeamModel> PotentialTies { get; set; } = new List<TeamModel>();
        public List<TeamModel> ActualTies { get; set; } = new List<TeamModel>();

        public int Number { get; set; }

        public double? WinsLossesTies
        {
            get
            {
                return CalculateWinsLossesTies(Wins, Losses, Ties, WinAlternatives, LossAlternatives, Forfeits, Games.Count(q => q.IsWinner.HasValue || q.IsTie.HasValue));
            }
        }

        public double GamesBack(List<TeamModel> otherTeams)
        {
            double gamesBack = 0;

            if (!otherTeams.All(t => t.Complete))
            {
                // Only Process Games Back If Games Still Need To Be Played

                foreach (var otherTeam in otherTeams.Where(t => t.Id != Id))
                {
                    var gamesBackByTeam = (((otherTeam.Wins ?? 0) - (Wins ?? 0)) +
                                           ((Losses ?? 0) - (otherTeam.Losses ?? 0))) / (double)2;
                    if (gamesBackByTeam > gamesBack)
                    {
                        gamesBack = gamesBackByTeam;
                    }
                }
            }

            return gamesBack;
        }

        public double? CalculateWinsLossesTies(
            int? wins,
            int? losses,
            int? ties,
            int? winAlternatives,
            int? lossesAlternatives,
            int? forfeits,
            int gamesWithWinOrTie)
        {
            if (wins.HasValue || losses.HasValue || ties.HasValue)
            {
                double? total = 0;

                if (!UseWinningPercentage)
                {
                    // By Points

                    if (!PointsForForfeit.HasValue) forfeits = 0; // No Points For Forfeit, Ignore
                    if (!PointsForWinAlternative.HasValue) winAlternatives = 0; // No Points For WinAlt, Ignore
                    if (!PointsForLossAlternative.HasValue) lossesAlternatives = 0; // No Points For LossAlt, Ignore

                    var regulationWins = Math.Max(0, (wins ?? 0) - (winAlternatives ?? 0));
                    var regulationLosses = Math.Max(0, (losses ?? 0) - (lossesAlternatives ?? 0) - (forfeits ?? 0));

                    if (PointsForWin.HasValue)
                        total += (regulationWins * PointsForWin);

                    if (ties.HasValue && PointsForTie.HasValue)
                        total += (ties.Value * PointsForTie);

                    if (PointsForLoss.HasValue)
                        total += (regulationLosses * PointsForLoss);

                    if (winAlternatives.HasValue && PointsForWinAlternative.HasValue)
                        total += (winAlternatives.Value * PointsForWinAlternative);

                    if (lossesAlternatives.HasValue && PointsForLossAlternative.HasValue)
                        total += (lossesAlternatives.Value * PointsForLossAlternative);

                    if (forfeits.HasValue && PointsForForfeit.HasValue)
                        total += (forfeits.Value * PointsForForfeit);
                }
                else
                {
                    // Winning Percentage, Wins = 1, Ties = .5

                    double? totalPoints = 0;

                    if (wins.HasValue)
                        totalPoints += wins.Value;

                    if (ties.HasValue)
                        totalPoints += (ties.Value * .5);

                    total = (double)totalPoints / gamesWithWinOrTie;
                }

                return total;
            }
            return null;
        }

        public int? Wins
        {
            get
            {
                if (Games.All(q => !q.IsWinner.HasValue))
                    return null;

                return Games.Count(q => q.IsWinner.HasValue && q.IsWinner.Value);
            }
        }

        public int? SetWins
        {
            get
            {
                if(ScoringSystem == ScoringSystem.Rally)
                {
                    var outcomes = GetSetWinsLosses(Games);

                    if(outcomes.Total > 0)
                    {
                        return outcomes.Wins;
                    }
                }

                return null;
            }
        }

        public int? SetLosses
        {
            get
            {
                if (ScoringSystem == ScoringSystem.Rally)
                {
                    var outcomes = GetSetWinsLosses(Games);

                    if (outcomes.Total > 0)
                    {
                        return outcomes.Losses;
                    }
                }

                return null;
            }
        }

        public int? WinAlternatives
        {
            get
            {
                if (Games.All(q => !q.IsWinner.HasValue))
                    return null;

                return Games.Where(GetAlternativeFilter()).Count(q => q.IsWinner.HasValue && q.IsWinner.Value);
            }
        }

        public int? Losses
        {
            get
            {
                if (Games.All(q => !q.IsWinner.HasValue))
                    return null;

                return Games.Count(q => q.IsWinner.HasValue && !q.IsWinner.Value);
            }
        }

        public int? LossAlternatives
        {
            get
            {
                if (Games.All(q => !q.IsWinner.HasValue))
                    return null;

                return Games.Where(GetAlternativeFilter()).Count(q => q.IsWinner.HasValue && !q.IsWinner.Value);
            }
        }

        public int? Forfeits
        {
            get
            {
                if (Games.All(q => !q.IsWinner.HasValue))
                    return null;

                return Games.Count(q => q.OpponentOutcomeType == ContestOutcomeType.Forfeit && q.IsWinner.HasValue && !q.IsWinner.Value);
            }
        }

        public Func<GameModel, bool> GetAlternativeFilter()
        {
            var @where = PredicateBuilder.New<GameModel>();

            if(SportType == SportType.WaterPolo)
            {
                where = where.And(q => (q.TeamScore.HasValue && (q.TeamScore.Value % 1) > 0) || (q.OpponentScore.HasValue && (q.OpponentScore.Value % 1) > 0));
            }
            else
            {
                where = where.And(t => false);
            }
            return where;
        }

        public int? Ties
        {
            get
            {
                if (Games.All(q => !q.OpponentScore.HasValue && !q.TeamScore.HasValue))
                    return null;

                return Games.Count(q => q.OpponentScore.HasValue && q.TeamScore.HasValue && q.OpponentScore == q.TeamScore);
            }
        }

        public bool Complete => Place.HasValue || (Games.Any() && Games.All(q => q.IsWinner.HasValue || q.IsTie.HasValue));
        public List<GameModel> UnplayedGames => Games.Where(q => !q.IsWinner.HasValue && !q.IsTie.HasValue).ToList();
        public List<GameModel> PlayedGames => Games.Where(q => q.IsWinner.HasValue || q.IsTie.HasValue).ToList();

        private int? GetCorrectScore(GameModel q, bool overall)
        {
            var pointDifference = q.PointDifferential;

            if(pointDifference.HasValue)
            {
                if (MaxPoints.HasValue && !overall)
                {
                    if (pointDifference.Value > MaxPoints)
                        return MaxPoints;

                    if (pointDifference.Value < -MaxPoints)
                        return -MaxPoints;
                }
            }

            return pointDifference;
        }

        public int? GetPointDifference(bool overall)
        {
            return GetPointDifferenceBetweenGames(Games, overall);
        }

        public int? GetPointDifferenceBetweenGames(List<GameModel> games, bool overall)
        {
            return games.Any(q => q.IsWinner.HasValue || q.IsTie.HasValue) ? games.Sum(q => GetCorrectScore(q, overall)) : null;
        }

        public int? GetPointDifference(List<GameModel> games, bool overall)
        {
            var points = GetPointDifferenceBetweenGames(games, overall);

            if (MaxPoints.HasValue)
            {
                foreach (var game in games)
                {
                    if (game.OutcomeType == ContestOutcomeType.Forfeit || game.OpponentOutcomeType == ContestOutcomeType.Forfeit)
                    {
                        if (points.HasValue)
                            points += game.OpponentOutcomeType == ContestOutcomeType.Forfeit ? -MaxPoints : MaxPoints;
                        else
                            points = game.OpponentOutcomeType == ContestOutcomeType.Forfeit ? -MaxPoints : MaxPoints;
                    }
                }
            }

            return points;
        }

        public int Position { get; set; }

        public int? MaxPoints { get; set; }
        public int? TeamId { get; set; }

        public bool PositionFound { get; set; }

        public int? PointDifference => GetPointDifference(Games, false);
        public int? OverallPointDifference => GetPointDifference(Games, true);
        public int? PointsAllowed => GetPointsAllowed(Games);
        public int? PointsScored => GetPointsScored(Games);
        public double? PointsScoredAverage => GetPointsScoredAverage(Games);
        public double? AveragePointsScored => GetAveragePointsScored(Games);
        public double? SetPercentage => GetSetPercentageRatio(Games, false);
        public double? SetRatio => GetSetPercentageRatio(Games, true);
        public double? PointPercentage => GetPointPercentage(Games);
        public double? PointRatio => GetPointRatio(Games);
        public bool UseWinningPercentage => !PointsForWin.HasValue && !PointsForTie.HasValue && !PointsForLoss.HasValue;

        public double? PointsForWin { get; set; }
        public double? PointsForTie { get; set; }
        public double? PointsForLoss { get; set; }
        public double? PointsForWinAlternative { get; set; }
        public double? PointsForLossAlternative { get; set; }
        public double? PointsForForfeit { get; set; }

        public SportType SportType { get; set; }
        public ScoringType ScoringType { get; set; }
        public ScoringSystem ScoringSystem { get; set; }

        public int? PointDifferenceTeamsInTie { get; set; }
        public bool Exhibition { get; set; }
        public int PoolId { get; set; }
        public int? Place { get; set; }
        public bool CoinFlip { get; set; }
        public int TiePosition { get; set; }

        public int? GetPointsAllowed(List<GameModel> games)
        {
            return games.Any(q => q.IsWinner.HasValue || q.IsTie.HasValue) ? games.Sum(q => q.OpponentPoints.Sum(t => t)) : (int?)null;
        }

        public int? GetPointsScored(List<GameModel> games)
        {
            return games.Any(q => q.TeamPoints.Any() || q.IsTie.HasValue) ? games.Sum(q => q.TeamPoints.Sum(t => t)) : (int?)null;
        }

        public double? GetAveragePointsScored(List<GameModel> games)
        {
            var pointsScored = 0;

            var validGames = games.Where(q => q.TeamPoints.Any()).ToList();

            if(validGames.Any())
            {
                pointsScored += validGames.Sum(q => q.TeamPoints.Sum(t => t));
            }

            // Add Forfeit Losses
            validGames.AddRange(games.Where(t => t.OpponentOutcomeType == ContestOutcomeType.Forfeit).ToList());

            if (!validGames.Any())
                return null;

            return pointsScored / (double)validGames.Count();
        }

        // For Hockey
        public double? GetPointsScoredAverage(List<GameModel> games)
        {
            if (PointsAllowed.HasValue && PointsScored.HasValue)
            {
                if ((PointsScored + PointsAllowed) == 0)
                    return 0;

                return PointsScored/(double?) (PointsScored + PointsAllowed);
            }
            return null;
        }

        public (int? Total, int Wins, int Losses) GetSetWinsLosses(List<GameModel> games)
        {
            int? totalSets = null;
            int totalSetWins = 0;
            int totalSetLosses = 0;

            if (Games.Any(q => q.IsWinner.HasValue))
            {
                foreach (var game in games)
                {
                    foreach (var teamSetPoints in game.TeamPoints.Select((item, index) => new { Points = item, Number = index + 1 }))
                    {
                        var opponentSetPoints = game.OpponentPoints[teamSetPoints.Number - 1];

                        if (teamSetPoints.Points > opponentSetPoints)
                        {
                            totalSetWins++;
                        }
                        else if (teamSetPoints.Points < opponentSetPoints)
                        {
                            totalSetLosses++;
                        }
                        totalSets = ++totalSets ?? 1;
                    }
                }
            }

            return (totalSets, totalSetWins, totalSetLosses);
        }

        public double? GetSetPercentageRatio(List<GameModel> games, bool ratio)
        {
            var outcomes = GetSetWinsLosses(games);

            if (outcomes.Total.HasValue)
            {
                if (ratio)
                {
                    // Ratio
                    return Math.Round(outcomes.Wins / (double)outcomes.Losses, 4);
                }
                else
                {
                    // Percentage
                    return Math.Round(outcomes.Wins / (double)outcomes.Total.Value, 4) * 100;
                }
            }

            return null;
        }

        public double? GetPointRatio(List<GameModel> games)
        {
            if (!PointsAllowed.HasValue || !PointsScored.HasValue)
                return null;

            return Math.Round(PointsScored.Value / (double)PointsAllowed.Value, 4);
        }

        public double? GetPointPercentage(List<GameModel> games)
        {
            if (!PointsAllowed.HasValue || !PointsScored.HasValue)
                return null;

            return Math.Round((PointsScored.Value / (double)(PointsScored.Value + PointsAllowed.Value)), 4) * 100;
        }

        public bool TiesHavePlace
        {
            get
            {
                return Place.HasValue && ActualTies.All(t => t.Place.HasValue);
            }
        }
    }
}
