﻿using Exposure.Common;
using Exposure.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Exposure.PoolStandingsProcessor.Model
{
    public class GameModel
    {
        public ContestOutcomeType? OpponentOutcomeType { get; set; }
        public int? OpponentDivisionTeamPoolId { get; set; }
        public double? OpponentScore { get; set; }

        public double? TeamScore { get; set; }
        public List<int> OpponentPoints { get; set; } = new List<int>();
        public List<int> TeamPoints { get; set; } = new List<int>();
        public ContestOutcomeType? OutcomeType { get; set; }
        public bool? HomeTeam { get; set; }

        public int? PointsScored => TeamPoints.Sum(t => t.To<int?>());
        public int? PointsAllowed => OpponentPoints.Sum(t => t.To<int?>());
        public int? PointDifferential => TeamPoints.Sum(t => t.To<int?>()) - OpponentPoints.Sum(t => t.To<int?>());

        public bool? IsWinner
        {
            get
            {
                if (OutcomeType.HasValue)
                    return true;

                if (OpponentOutcomeType.HasValue)
                    return false;

                // Has Score And No Tie
                if (OpponentScore.HasValue && TeamScore.HasValue && (!IsTie.HasValue || !IsTie.Value))
                {
                    return OpponentScore.Value < TeamScore.Value;
                }

                return null;
            }   
        }

        public bool? IsTie
        {
            get
            {
                if (OutcomeType.HasValue)
                    return false;

                if (OpponentOutcomeType.HasValue)
                    return false;

                // Has Score And No Tie
                if (OpponentScore.HasValue && TeamScore.HasValue && OpponentScore.Value == TeamScore.Value)
                {
                    return true;
                }

                return null;
            }   
        }

        public int Id { get; set; }

        public DateTime? Date { get; set; }
        public DateTime? Time { get; set; }
        public int? TeamPoolId { get; set; }
    }
}
