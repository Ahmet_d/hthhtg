﻿using Exposure.PoolStandingsProcessor.Model;
using System.Collections.Generic;

namespace Exposure.PoolStandingsProcessor.Model.HeadToHead
{
    public class HeadToHeadStandingsModel
    {        
        public double? OrderNo { get; set; }
        public List<TeamModel> Teams { get; set; }
        public bool PositionFound => OrderNo.HasValue && Teams.Count == 1;
    }
}
