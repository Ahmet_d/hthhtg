﻿
namespace Exposure.PoolStandingsProcessor.Model.HeadToHead
{
    public enum HeadToHeadComparisonOperator
    {
        None = '-',
        GreaterThan = '>',
        LessThan = '<',
        Equal = '='
    }
}
