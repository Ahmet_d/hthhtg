﻿namespace Exposure.PoolStandingsProcessor.Model.HeadToHead
{
    public class HeadToHeadGraphModel
    {
        public int Team1 { get; set; }
        public int Team2 { get; set; }
        public HeadToHeadComparisonOperator Compare { get; set; }
    }
}
