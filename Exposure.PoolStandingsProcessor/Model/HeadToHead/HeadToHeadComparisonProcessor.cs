﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Exposure.PoolStandingsProcessor.Model.HeadToHead
{
    public class HeadToHeadComparisonProcessor
    {             
        private List<List<int>> FindUnmatchedPairs (List<HeadToHeadGraphModel> graph, List<TeamModel> teams)
        {
            var unmatchedpairs = new List<List<int>>();
            var teamCount = teams.Count;
            for (var left = 0; left < teamCount - 1; left++)
            {
                for (var right = left + 1; right < teamCount; right++)
                {
                    var count = graph.Count(x => (x.Team1 == teams[left].Id && x.Team2 == teams[right].Id)
                                    || (x.Team2 == teams[left].Id && x.Team1 == teams[right].Id));
                    if (count == 0)
                    {
                        unmatchedpairs.Add(new List<int> { teams[left].Id, teams[right].Id });
                    }

                }
            }
            return unmatchedpairs;
        }
               
        /* 
         * HTHType: 0: Reduction for Normal HTH, 1: Reduction for HTH Differential
        */ 
        private List<HeadToHeadGraphModel> HeadToHeadReduction(List<TeamModel> teams,int HTHType)
        {
            //get each two team slice and compare:
            int teamCount = teams.Count;
            var graph = new List<HeadToHeadGraphModel>();
           
            for (int i = 0; i < teamCount - 1; i++)
            {
                for (int j = i + 1; j < teamCount; j++)
                {
                    var playedgames = teams[i].PlayedGames.Where(x => x.OpponentDivisionTeamPoolId == teams[j].Id).ToList();

                    int playedcount = playedgames.Count();

                    if (playedcount != 0)
                    {
                        if (HTHType == 1) //comparison for HTH differantial
                        {
                            var scorei = playedgames.Sum(x => x.TeamScore);
                            var scorej = playedgames.Sum(x => x.OpponentScore);
                            if (scorei > scorej)
                            {
                                graph.Add(new HeadToHeadGraphModel { Team1 = teams[i].Id, Team2 = teams[j].Id, Compare = HeadToHeadComparisonOperator.GreaterThan });
                            }
                            else if (scorei < scorej)
                            {
                                graph.Add(new HeadToHeadGraphModel { Team1 = teams[j].Id, Team2 = teams[i].Id, Compare = HeadToHeadComparisonOperator.GreaterThan });
                            }
                            else
                            {
                                graph.Add(new HeadToHeadGraphModel { Team1 = teams[i].Id, Team2 = teams[j].Id, Compare = HeadToHeadComparisonOperator.Equal });
                            }
                        }
                        else
                        {
                            int wincount = playedgames.Count(x => x.IsWinner.HasValue && x.IsWinner.Value);
                            int tiecount = playedgames.Count(x => x.IsTie.HasValue && x.IsTie.Value);
                            int loosecount = playedcount - wincount - tiecount;

                            if (wincount > loosecount)
                            {
                                graph.Add(new HeadToHeadGraphModel { Team1 = teams[i].Id, Team2 = teams[j].Id, Compare = HeadToHeadComparisonOperator.GreaterThan });
                            }
                            else if (wincount < loosecount)
                            {
                                graph.Add(new HeadToHeadGraphModel { Team1 = teams[j].Id, Team2 = teams[i].Id, Compare = HeadToHeadComparisonOperator.GreaterThan });
                            }
                            else if (wincount == loosecount)
                            {
                                graph.Add(new HeadToHeadGraphModel { Team1 = teams[i].Id, Team2 = teams[j].Id, Compare = HeadToHeadComparisonOperator.Equal });
                            }
                        }
                    }
                }
            }
            graph = graph.OrderBy(x=> x.Compare).OrderBy(x=> x.Team1).ToList();
            return graph;
           
        }

        private void GetPathsOfTeam(List<HeadToHeadGraphModel> graph , List<int> curList,List<List<int>> trees, int teamid)
        {
            var nodes = graph.Where(x => x.Compare == HeadToHeadComparisonOperator.GreaterThan).Where(x => x.Team1 == teamid).ToList();
            if (nodes.Count >= 1) //clone current list count times and start new recur for each list teamid
            {
                curList.Add(teamid);                
                for ( int i = 0; i < nodes.Count;i++)
                {
                    if (curList.Contains(nodes[i].Team2)) //circular reference
                    {
                        //move circular reference items from greater to equals:                        
                        var gm1 = graph.Where(x => x.Team1 == nodes[i].Team1 && x.Team2 == nodes[i].Team2
                                                    && x.Compare == HeadToHeadComparisonOperator.GreaterThan).FirstOrDefault();
                        if (gm1 != null) gm1.Compare = HeadToHeadComparisonOperator.Equal;
                        for (var k = curList.Count - 1; k > 0 && curList[k] != nodes[i].Team2; k--)
                        {
                            var gm = graph.Where(x => x.Team1 == curList[k - 1] && x.Team2 == curList[k]
                                                    && x.Compare == HeadToHeadComparisonOperator.GreaterThan).FirstOrDefault();
                            if (gm != null) gm.Compare = HeadToHeadComparisonOperator.Equal;

                        }
                        //curList.Clear(); //do not add circular alternative
                        //if (!IsSequentialSubset(curList,trees)) trees.Add(curList);
                    }
                    else
                    {
                        var tmpList = new List<int>();
                        tmpList.AddRange(curList);
                        GetPathsOfTeam(graph, tmpList, trees, nodes[i].Team2);
                    }
                }               
            } 
            else if (nodes.Count == 0) // add teamid to list return all lists
            {
                if (curList.Count != 0)
                {
                    curList.Add(teamid);
                    if (!IsSequentialSubset(curList, trees)) trees.Add(curList);
                }
            }
            return;
        }

        private bool IsSequentialSubset(List<int> subset, List<List<int>> coll)
        {
            var strsub = String.Join(",", subset);           
            foreach (List<int> l in coll)
            {
                string strsuper = String.Join(",", l);
                if (strsuper.Contains(strsub)) return true;
            }
            return false;
        }
        private bool IsSubTree(List<int> subset, List<List<int>> coll)
        {            
            foreach (List<int> l in coll)
            {
                List<int> tmp = l.Except(subset).ToList();
                List<int> tmp2 = l.Except(tmp).ToList();
                if (subset.SequenceEqual(tmp2)) return true;
            }
            return false;
        }

        /* Rule no 1: Decription:
         *  //1-Is there any ID to move to the lower step: ex: case:33
            // I have beaten you step down from my side
            // if any element in the queue is smaller than any element in the same queue
            // then move thatones to the lower queue
        */
        private void HeadToHeadNormalRuleNo1(List<List<int>> orderques_orj,List<HeadToHeadGraphModel> graph_greaters)
        {           
            for (var i = 0; i < orderques_orj.Count; i++)
            {
                var curqueitems = new List<int>(orderques_orj[i].Distinct());
                for (var curleft = 0; curleft < curqueitems.Count - 1; curleft++)
                {
                    for (var curright = curleft + 1; curright < curqueitems.Count; curright++)
                    {
                        var leftissmaller = graph_greaters.Count(x => x.Team1 == curqueitems[curright] && x.Team2 == curqueitems[curleft]) >= 1 ? true : false;
                        var rightissmaller = graph_greaters.Count(x => x.Team1 == curqueitems[curleft] && x.Team2 == curqueitems[curright]) >= 1 ? true : false;

                        if (i != orderques_orj.Count - 1 && (leftissmaller || rightissmaller))
                        {
                            var itemtomove = leftissmaller ? curqueitems[curleft] : curqueitems[curright];                            
                            orderques_orj[i].Remove(itemtomove);
                            if (!orderques_orj[i + 1].Contains(itemtomove)) orderques_orj[i + 1].Add(itemtomove);
                        }
                    }
                }
            }
        }
       
        /* Rule No 2 Decription:
         * //2- IS there any ID to get from the next step: ex: case 31
            // I have beaten you come down to my side
            // take the ones from upper which is beaten by lower
        */
        private void HeadToHeadNormalRuleNo2(List<List<int>> orderques_orj, List<HeadToHeadGraphModel> graph_greaters)
        {
            for (var i = 0; i < orderques_orj.Count; i++)
            {
                if (i != orderques_orj.Count - 1)
                {
                    var curqueitems = new List<int>(orderques_orj[i].Distinct());
                    var nextqueitems = new List<int>(orderques_orj[i + 1].Distinct());
                    for (var next = 0; next < nextqueitems.Count; next++)
                    {
                        for (var cur = 0; cur < curqueitems.Count; cur++)
                        {
                            var nextisbigger = graph_greaters.Count(x => x.Team1 == nextqueitems[next]
                                                         && x.Team2 == curqueitems[cur]) >= 1 ? true : false;
                            if (nextisbigger)
                            {                              
                                if (!orderques_orj[i + 1].Contains(curqueitems[cur])) orderques_orj[i + 1].Add(curqueitems[cur]);
                                orderques_orj[i].Remove(curqueitems[cur]);
                            }
                        }
                    }
                }
            }
        }
       
        /* Rule Decription
         *  //3- Is there any ID to move up ex cases:23,25,31
            // You havent beaten me I am coming up to your side
            // Take the ones from lower which has not beaten any one the upper teams:
        */
        private void HeadToHeadNormalRuleNo3(List<List<int>> orderques_orj, List<HeadToHeadGraphModel> graph_greaters)
        {           
            for (var i = 0; i < orderques_orj.Count; i++)
            {
                if (i != orderques_orj.Count - 1)
                {
                    var curqueitems = new List<int> (orderques_orj[i].Distinct());
                    var nextqueitems = new List<int>(orderques_orj[i + 1].Distinct());
                    for (var next = 0; next < nextqueitems.Count; next++)
                    {
                        for (var cur = 0; cur < curqueitems.Count; cur++)
                        {
                            bool nextisnotbeaten = graph_greaters.Count(x => x.Team1 == curqueitems[cur]
                                                         && x.Team2 == nextqueitems[next]) == 0 ? true : false;
                            if (nextisnotbeaten)
                            {                             
                                if (!orderques_orj[i].Contains(nextqueitems[next])) orderques_orj[i].Add(nextqueitems[next]);
                                orderques_orj[i + 1].Remove(nextqueitems[next]);
                            }
                        }
                    }
                }
            }
        }
        
        private List<HeadToHeadStandingsModel> HeadToHeadBaseCompare(List<TeamModel> teams,int HTHType)
        {
            var ordernumbers = new Dictionary<int, int>();

            //get reduction graph:
            var graph = HeadToHeadReduction(teams,HTHType).ToList();
            var graph_greaters = graph.Where(x => x.Compare == HeadToHeadComparisonOperator.GreaterThan).ToList();
            var graph_equals = graph.Where(x => x.Compare == HeadToHeadComparisonOperator.Equal).ToList();

            //get unique team IDs in the graph
            var graphteams = new List<int>();

            if (graph.Count != 0)
            {
                graphteams.AddRange(graph.Select(x => x.Team1));
                graphteams.AddRange(graph.Select(x => x.Team2));
                graphteams = graphteams.Distinct().ToList();
            }

            #region Direct exit situations:
            //Ambiguity-1: Is there any team that have no matches:
            if (graphteams.Count != teams.Count)
            {
                var tmpres = new List<HeadToHeadStandingsModel>
                {
                    new HeadToHeadStandingsModel { OrderNo = null, Teams = teams }
                };
                return tmpres;
            }

            //Ambiguity-2: Not enough matches between teams: case 18: 1>2,3>4
            if (graph.Count < teams.Count - 1)
            {
                var tmpres = new List<HeadToHeadStandingsModel>
                {
                    new HeadToHeadStandingsModel { OrderNo = null, Teams = teams }
                };
                return tmpres;
            }
            #endregion

            //for each team1 determine all paths to the end and paths to seperate lists:                   
            var trees = new List<List<int>>();
            foreach (var team in teams)
            {               
                GetPathsOfTeam(graph_greaters, new List<int>(), trees, team.Id);                
            }
            //graph datas may change in gatpaths because of circular references so refresh:
            graph_greaters = graph.Where(x => x.Compare == HeadToHeadComparisonOperator.GreaterThan).ToList();
            graph_equals = graph.Where(x => x.Compare == HeadToHeadComparisonOperator.Equal).ToList();

            // Direct Exit-1: IF graph contains only equalities between teams, so return all teams order=1
            if (graph_greaters.Count == 0 && graph_equals.Count != 0)
            {
                var tmpres = new List<HeadToHeadStandingsModel>
                {
                    new HeadToHeadStandingsModel { OrderNo = 1,Teams = teams }
                };               
                return tmpres;
            }

            //remove subset ordering alternatives:
            var finaltrees = new List<List<int>>(trees);
            for (int i = 0; i < trees.Count; i++)
            {                
                for (int j = 0; j < trees.Count; j++)
                {
                    if (j != i)
                    {
                        if (trees[i].SequenceEqual(trees[j].Except(trees[j].Except(trees[i])).ToList()))
                        {                           
                            finaltrees.Remove(trees[i]);
                            break;
                        }
                    }
                }
            }
            
            //make ID list of IDs for each order number:
            int maxlevel = finaltrees.Any() ? finaltrees.Max(x => x.Count) : 0;
            var orderques = new List<List<int>>(maxlevel);
            for (int i= 1; i <= maxlevel; i++) { orderques.Add(new List<int>()); }
            foreach (var t in finaltrees)
            {
                for (int i = 0; i < t.Count;i++)
                {
                    if(!orderques[i].Contains(t[i])) orderques[i].Add(t[i]);
                }
            }
            // Ambiguity-3: not enough games and no relation. Return all teams as order=null ex case: 19
            // IF there is no shared team in the relation trees of unmatched teams then return all null
            var unMatchedPairs = FindUnmatchedPairs(graph, teams);
            if (unMatchedPairs.Count > 0)
            {
                foreach (var pr in unMatchedPairs)
                {
                    //get teamsids of all trees that first team in and equal relations
                    var teamsof1 = new List<int>() { pr[0] };
                    foreach (var gm in graph_equals.Where(x => x.Team1 == pr[0] || x.Team2 == pr[0]))
                    {
                        if (gm.Team1 != pr[0]) teamsof1.Add(gm.Team1);
                        if (gm.Team2 != pr[0]) teamsof1.Add(gm.Team2);
                    }
                    teamsof1.AddRange(finaltrees.Where(x => x.Intersect(teamsof1).Count() > 0).SelectMany(x => x)
                                    .Where(x => x != pr[0]).ToList());
                    teamsof1 = teamsof1.Where(x=> x != pr[0]).Distinct().ToList();

                    //get teamsids of all trees that second team in
                    var teamsof2 = new List<int>() { pr[1] };
                    foreach (var gm in graph_equals.Where(x => x.Team1 == pr[1] || x.Team2 == pr[1]))
                    {
                        if (gm.Team1 != pr[1]) teamsof2.Add(gm.Team1);
                        if (gm.Team2 != pr[1]) teamsof2.Add(gm.Team2);
                    }
                    teamsof2.AddRange(finaltrees.Where(x => x.Intersect(teamsof2).Count() > 0).SelectMany(x => x)
                                           .Where(x => x != pr[1]).ToList());
                    teamsof2 = teamsof2.Where(x => x != pr[1]).Distinct().ToList();

                    //find intersect element count
                    var intersect = teamsof1.Intersect(teamsof2).Count();
                    if (intersect == 0) //if there is intersecting elements then it is ambiguous
                    {
                        var tmpres = new List<HeadToHeadStandingsModel>
                        {
                            new HeadToHeadStandingsModel { OrderNo = null, Teams = teams }
                        };
                        return tmpres;
                    }
                }
            }

            //Deciding process:
            // Apply rule 1
            HeadToHeadNormalRuleNo1(orderques, graph_greaters);
            // Apply rule 2
            HeadToHeadNormalRuleNo2(orderques, graph_greaters);
            // Apply rule 3
            HeadToHeadNormalRuleNo3(orderques, graph_greaters);

            //Give final situation order numbers:
            for (var i = 0; i < orderques.Count; i++)
            {
                foreach (var id in orderques[i].Distinct()) 
                {
                    
                      if (!ordernumbers.ContainsKey(id))
                            ordernumbers.Add(id, 0);
                        ordernumbers[id] = ordernumbers[id] + i + 1;
                }             
            }

            ////give the equals same numbers:            
            foreach (HeadToHeadGraphModel gme in graph_equals)
            {
                if (ordernumbers.ContainsKey(gme.Team1) && !ordernumbers.ContainsKey(gme.Team2))
                {
                    ordernumbers.Add(gme.Team2, ordernumbers[gme.Team1]);
                }
                if (!ordernumbers.ContainsKey(gme.Team1) && ordernumbers.ContainsKey(gme.Team2))
                {
                    ordernumbers.Add(gme.Team1, ordernumbers[gme.Team2]);
                }
            }

            //Rearrange skipping order numbers:
            var values = ordernumbers.Values.Distinct().OrderBy(x => x).ToArray();            
            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] != i + 1)
                {
                    var keys = ordernumbers.Where(x => x.Value == values[i]).Select(x=> x.Key).ToArray();
                    foreach (var key in keys)
                    {
                        ordernumbers[key] = i + 1;
                    }
                }
            }

            //ALL DONE!! return the result
            var res = from g in ordernumbers
                      group g by g.Value into gr
                      orderby gr.Key
                      select new HeadToHeadStandingsModel
                      {
                          OrderNo = gr.Key,
                          Teams = (from tm in gr
                                   select teams.FirstOrDefault(x => x.Id == tm.Key)
                                   ).OrderBy(x=> x.Id).ToList()
                      };
            return res.ToList();
        }


        public List<HeadToHeadStandingsModel> HeadToHeadNormalCompare(List<TeamModel> teams)
        {
            return HeadToHeadBaseCompare(teams, 0);
        }

        public List<HeadToHeadStandingsModel> HeadToHeadDifferentialCompare(List<TeamModel> teams)
        {
            return HeadToHeadBaseCompare(teams, 1);
        }

        public List<HeadToHeadStandingsModel> HeadToHeadTwoOnlyCompare(List<TeamModel> teams)
        {
            if (teams.Count != 2)
            {
                var tmpres = new List<HeadToHeadStandingsModel>
                {
                    new HeadToHeadStandingsModel { OrderNo = null, Teams = teams }
                };
                return tmpres;
            }
            else
            {
                return HeadToHeadBaseCompare(teams, 0);
            }
        }

        public List<HeadToHeadStandingsModel> HeadToHeadAllPlayedCompare(List<TeamModel> teams)
        {
            var allteamids = teams.Select(x => x.Id).ToList();
            var allplayed = true;
            for (var i = 0; i < teams.Count; i++)
            {
                var playedteamids = teams[i].Games
                            .Where(x=> x.OpponentScore.HasValue && x.TeamScore.HasValue)
                            .Select(x => x.OpponentDivisionTeamPoolId.Value)
                            .Distinct().ToList();
                playedteamids.Add(teams[i].Id); // add itself for below compare
                if (allteamids.Except(playedteamids).Count() > 0)
                {
                    allplayed = false;
                    break;
                }                
            }
            if (allplayed)
            {
                return HeadToHeadBaseCompare(teams, 0);               
            }
            else
            {
                var tmpres = new List<HeadToHeadStandingsModel>
                {
                    new HeadToHeadStandingsModel { OrderNo = null, Teams = teams }
                };
                return tmpres;
            }
        }

        public List<HeadToHeadStandingsModel> HeadToGroupCompare(List<TeamModel> teams)
        {
            //take all the between matches
            //calculate the winning percentage of each team from between games
            // sort and group by wp and return the result

            var betweenteams = new List<TeamModel>();
            betweenteams.AddRange(teams);
            int[] allteamIDs = teams.Select(x => x.Id).ToArray();

            //get only games beetween group:
            foreach (var tm in betweenteams)
            {
                var opponentIDs = allteamIDs.Except(new int[] { tm.Id }).ToList();
                var games = tm.Games.Where(x => opponentIDs.Contains(x.OpponentDivisionTeamPoolId.Value)).ToList();
                if (games.Count == 0) //return all null if there is a team that didnt have any matches in group
                {
                    var tmpres = new List<HeadToHeadStandingsModel>
                        {
                            new HeadToHeadStandingsModel { OrderNo = null, Teams = teams }
                        };
                    return tmpres;
                }
            }

            var groups = from wp in
                            (from t in betweenteams
                             let winsValue = (t.Wins ?? 0)
                             let tiesValue = (t.Ties ?? 0) * .5
                             select new
                             {
                                 WinningPercentage = (double)(winsValue + tiesValue) / (double)((t.Wins ?? 0) + (t.Ties ?? 0) + (t.Losses ?? 0)),
                                 Teams = t
                             })
                         group wp by wp.WinningPercentage into gr
                         orderby gr.Key descending
                         select gr;

            var result = new List<HeadToHeadStandingsModel>();
            int counter = 1;
            foreach (var g in groups)
            {
                result.Add(new HeadToHeadStandingsModel
                {
                    OrderNo = counter,
                    Teams = (from r in g select r.Teams).ToList()
                });
                counter++;
            }
            return result;
        }
    }
}
