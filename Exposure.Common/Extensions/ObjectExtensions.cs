﻿using System;
using System.ComponentModel;

namespace Exposure.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static T To<T>(this object obj)
        {
            Type t = typeof(T);
            Type u = Nullable.GetUnderlyingType(t);

            if (obj == null)
                return default(T);

            if (u != null)
            {
                if (obj.ToString().Length == 0)
                    return default(T);

                return InnerConvert<T>(obj, u);
            }

            return InnerConvert<T>(obj, t);
        }

        private static T InnerConvert<T>(object obj, Type u)
        {
            try
            {
                return (T)Convert.ChangeType(obj, u);
            }
            catch (Exception ex) when (ex is FormatException || ex is InvalidCastException)
            {
                try
                {
                    return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(obj.ToString());
                }
                catch
                {
                    return default(T);
                }
            }
        }

    }
}
