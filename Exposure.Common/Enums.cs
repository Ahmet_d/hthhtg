
using Exposure.Common.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Exposure.Common
{
    public enum ScoringSystem
    {
        Score = 0,
        Rally = 1
    }

    public enum ScoreType
    {
        Score = 0,
        Set = 1
    }

    [DataContract(Namespace = "")]
    public enum ScoringType
    {
        [EnumMember(Value = "1"), Display(Name = "Points")]
        Points = 1,
        [EnumMember(Value = "2"), Display(Name = "Goals")]
        Goals = 2,
        [EnumMember(Value = "3"), Display(Name = "Runs")]
        Runs = 3
    }

    public enum ContestOutcomeType
    {
        [EnumMember(Value = "1"), Display(Name = "Decision", ShortName = "DEC")]
        Decision = 1,
        [EnumMember(Value = "2"), Display(Name = "Major Decision", ShortName = "MD")]
        MajorDecision = 2,
        [EnumMember(Value = "3"), Display(Name = "Technical Fall", ShortName = "TF")]
        TechnicalFall = 3,
        [EnumMember(Value = "4"), Display(Name = "Fall", ShortName = "F")]
        Fall = 4,
        [EnumMember(Value = "5"), Display(Name = "Disqualification", ShortName = "DQ")]
        Disqualification = 5,
        [EnumMember(Value = "6"), Display(Name = "Injury Default", ShortName = "DEF")]
        InjuryDefault = 6,
        [EnumMember(Value = "7"), Display(Name = "Forfeit", ShortName = "FF")]
        Forfeit = 7,
        [EnumMember(Value = "8"), Display(Name = "Medical Forfeit", ShortName = "MFF")]
        MedicalForfeit = 8,
        [EnumMember(Value = "9"), Display(Name = "DQ Weight Mgmt", ShortName = "DQ WT")]
        DQWeightMgmt = 9,
        [EnumMember(Value = "10"), Display(Name = "DQ Medical Prtcl", ShortName = "DQ MED")]
        DQMedicalPrtcl = 10,
        [EnumMember(Value = "11"), Display(Name = "Rule", ShortName = "RULE")]
        Rule = 11,
        [EnumMember(Value = "12"), Display(Name = "Sudden Victory - 1", ShortName = "SV-1")]
        SuddenVictory1 = 12,
        [EnumMember(Value = "13"), Display(Name = "Sudden Victory - 2", ShortName = "SV-2")]
        SuddenVictory2 = 13,
        [EnumMember(Value = "14"), Display(Name = "Overtime", ShortName = "OT")]
        Overtime = 14,
        [EnumMember(Value = "15"), Display(Name = "Tie Breaker - 1", ShortName = "TB-1")]
        TieBreaker1 = 15,
        [EnumMember(Value = "16"), Display(Name = "Tie Breaker - 2", ShortName = "TB-2")]
        TieBreaker2 = 16,
        [EnumMember(Value = "17"), Display(Name = "Ultimate Tie Breaker", ShortName = "UTB")]
        UltimateTieBreaker = 17
    }

    [DataContract(Namespace = "")]
    public enum SportType
    {
        [EnumMember(Value = "0"), Display(Name = "Other"), EnumOrder(Order = 0)]
        Other = 0,
        [EnumMember(Value = "1"), Display(Name = "Basketball"), EnumOrder(Order = 100)]
        Basketball = 1,
        [EnumMember(Value = "2"), Display(Name = "Football"), EnumOrder(Order = 250)]
        Football = 2,
        [EnumMember(Value = "3"), Display(Name = "Volleyball"), EnumOrder(Order = 600)]
        Volleyball = 3,
        [EnumMember(Value = "4"), Display(Name = "Hockey"), EnumOrder(Order = 350)]
        Hockey = 4,
        [EnumMember(Value = "5"), Display(Name = "Baseball"), EnumOrder(Order = 50)]
        Baseball = 5,
        [EnumMember(Value = "6"), Display(Name = "Lacrosse"), EnumOrder(Order = 450)]
        Lacrosse = 6,
        [EnumMember(Value = "7"), Display(Name = "Soccer"), EnumOrder(Order = 500)]
        Soccer = 7,
        [EnumMember(Value = "8"), Display(Name = "Softball"), EnumOrder(Order = 550)]
        Softball = 8,
        [EnumMember(Value = "9"), Display(Name = "Futsal"), EnumOrder(Order = 300)]
        Futsal = 9,
        [EnumMember(Value = "10"), Display(Name = "Kickball"), EnumOrder(Order = 400)]
        Kickball = 10,
        [EnumMember(Value = "11"), Display(Name = "Water Polo"), EnumOrder(Order = 650)]
        WaterPolo = 11,
        [EnumMember(Value = "12"), Display(Name = "Wrestling"), EnumOrder(Order = 700)]
        Wrestling = 12,
        [EnumMember(Value = "13"), Display(Name = "Field Hockey"), EnumOrder(Order = 200)]
        FieldHockey = 13,
        [EnumMember(Value = "14"), Display(Name = "Cornhole"), EnumOrder(Order = 100)]
        Cornhole = 14,
        [EnumMember(Value = "15"), Display(Name = "Dodgeball"), EnumOrder(Order = 150)]
        Dodgeball = 15
    }
}
