﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exposure.PoolStandingsProcessor.Model.HeadToHead;
using Exposure.PoolStandingsProcessor.Model;
using System.Collections.Generic;
using System.Linq;

namespace Exposure.Tests
{
    [TestClass]
    public class HTHDiffTests
    {
        private HTHDiff_DataCreator _dataCreator;
        private HeadToHeadComparisonProcessor _HTHComparison;

        [TestInitialize]
        public void Setup()
        {
            _dataCreator = new HTHDiff_DataCreator();
            _HTHComparison = new HeadToHeadComparisonProcessor();
        }

        //1: 1 win, +10 points; 2: 1 win, +5 points
        [TestMethod]
        public void TestCase3_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHDiff(3);

            //act:
            var result = _HTHComparison.HeadToHeadDifferentialCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }
        //1: 1 win, +10 points; 2: 2 wins, +5 points; 2 wins 3 
        [TestMethod]
        public void TestCase6_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHDiff(6);

            //act:
            var result = _HTHComparison.HeadToHeadDifferentialCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);
        }
    }
}
