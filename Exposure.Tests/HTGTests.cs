﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exposure.PoolStandingsProcessor.Model.HeadToHead;
using System.Linq;

namespace Exposure.Tests
{
    [TestClass]
    public class HTGTests
    {
        private HTGDataCreator _dataCreator;
        private HeadToHeadComparisonProcessor _HTHComparison;

        [TestInitialize]
        public void Setup()
        {
            _dataCreator = new HTGDataCreator();
            _HTHComparison = new HeadToHeadComparisonProcessor();
        }

        // 1>2,1>3,3>1 :: 1: 2 win, 1 loose; 2: 1loose; 3: 1 win,1 loose
        [TestMethod]
        public void TestCase14_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(14);

            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(2, result[2].Teams[0].Id);
            
        }
        [TestMethod]
        public void TestCase12_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(12);

            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(2, result[2].Teams[0].Id);

        }
        [TestMethod]
        public void TestCase16_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(16);

            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(2, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase18_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(18);

            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.IsTrue(result[0].Teams.Select(x => x.Id).Except(new int[] { 1, 3 }).Count() == 0);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.IsTrue(result[1].Teams.Select(x => x.Id).Except(new int[] { 2, 4 }).Count() == 0);
        }
        [TestMethod]
        public void TestCase19_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(19);

            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.IsTrue(result[0].Teams.Select(x => x.Id).Except(new int[] { 1, 3 }).Count() == 0);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.IsTrue(result[1].Teams.Select(x => x.Id).Except(new int[] { 2, 4 }).Count() == 0);
        }
        [TestMethod]
        public void TestCase20_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(20);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(2, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.IsTrue(result[1].Teams.Select(x => x.Id).Except(new int[] { 1, 4 }).Count() == 0);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase22_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(22);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.IsTrue(result[1].Teams.Select(x => x.Id).Except(new int[] { 3, 2 }).Count() == 0);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(4, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase23_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(23);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(2, result[2].Teams.Count);
            Assert.IsTrue(result[2].Teams.Select(x => x.Id).Except(new int[] { 3, 4 }).Count() == 0);          
        }
        [TestMethod]
        public void TestCase25_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(25);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);            
            Assert.AreEqual(3, result[0].Teams[1].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(4, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase27_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(27);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(4, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);            
            Assert.AreEqual(3, result[1].Teams[1].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(2, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase29_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(29);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.IsTrue(result[1].Teams.Select(x => x.Id).Except(new int[] { 3,4 }).Count() == 0);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(2, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase32_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(32);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(5, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(4, result[2].Teams[0].Id);
            Assert.AreEqual(4, result[3].OrderNo);
            Assert.AreEqual(1, result[3].Teams.Count);
            Assert.AreEqual(3, result[3].Teams[0].Id);
            Assert.AreEqual(5, result[4].OrderNo);
            Assert.AreEqual(1, result[4].Teams.Count);
            Assert.AreEqual(2, result[4].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase33_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(33);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(5, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(4, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);            
            Assert.AreEqual(3, result[2].Teams[0].Id);

            Assert.AreEqual(4, result[3].OrderNo);
            Assert.AreEqual(1, result[3].Teams.Count);
            Assert.AreEqual(1, result[3].Teams[0].Id);

            Assert.AreEqual(5, result[4].OrderNo);
            Assert.AreEqual(1, result[4].Teams.Count);
            Assert.AreEqual(2, result[4].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase34_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(34);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(6, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(5, result[1].Teams.Count);
            Assert.IsTrue(result[1].Teams.Select(x => x.Id).Except(new int[] { 1,2,3, 4,5 }).Count() == 0);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(7, result[2].Teams[0].Id);
        }
        
        #region Unmodified
        [TestMethod]
        public void TestCase1_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(1);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
        }
        [TestMethod]
        public void TestCase2_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(2);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase3_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(3);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
        }
        [TestMethod]
        public void TestCase4_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(4);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }
        [TestMethod]
        public void TestCase5_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(5);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }
        [TestMethod]
        public void TestCase6_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(6);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase7_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(7);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(3, result[0].Teams[1].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase8_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(8);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);
        }
        [TestMethod]
        public void TestCase9_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(9);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase10_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(10);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);
        }
        [TestMethod]
        public void TestCase11_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(11);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(3, result[0].Teams[1].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase13_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(13);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }
        [TestMethod]
        public void TestCase15_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(15);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.IsTrue(result[0].Teams.Select(x => x.Id).Except(new int[] { 1,2,3 }).Count() == 0);
        }
        [TestMethod]
        public void TestCase17_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(17);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
            Assert.AreEqual(4, result[0].Teams[3].Id);
        }
        [TestMethod]
        public void TestCase21_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(21);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
            Assert.AreEqual(4, result[0].Teams[3].Id);
        }      
        [TestMethod]
        public void TestCase24_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(24);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(4, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase26_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(26);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(2, result[0].Teams[0].Id);
            Assert.AreEqual(4, result[0].Teams[1].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(1, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase28_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(28);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(2, result[2].Teams.Count);
            Assert.AreEqual(4, result[2].Teams[0].Id);
            Assert.AreEqual(5, result[2].Teams[1].Id);
        }
        [TestMethod]
        public void TestCase30_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(30);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(4, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(3, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);
            Assert.AreEqual(2, result[1].Teams[1].Id);
            Assert.AreEqual(3, result[1].Teams[2].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(5, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase31_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTG(31);
            //act:
            var result = _HTHComparison.HeadToGroupCompare(input);
            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(4, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(3, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);
            Assert.AreEqual(5, result[1].Teams[2].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(2, result[2].Teams[0].Id);
        }
                        
        #endregion


    }
}
