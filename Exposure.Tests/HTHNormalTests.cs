using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exposure.PoolStandingsProcessor.Model.HeadToHead;
using Exposure.PoolStandingsProcessor.Model;
using System.Collections.Generic;
using System.Linq;

namespace Exposure.Tests
{
    [TestClass]
    public class HTHNormalTests
    {
        private HTHNormal_DataCreator _dataCreator;
        private HeadToHeadComparisonProcessor _HTHComparison;

        [TestInitialize]
        public void Setup()
        {
            _dataCreator = new HTHNormal_DataCreator();
            _HTHComparison = new HeadToHeadComparisonProcessor();
        }

        [TestMethod]
        public void TestCase1_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(1);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
        }
      
        [TestMethod]
        public void TestCase2_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(2);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase3_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(3);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
        }

        [TestMethod]
        public void TestCase4_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(4);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }

        [TestMethod]
        public void TestCase5_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(5);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }

        [TestMethod]
        public void TestCase6_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(6);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase7_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(7);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(3, result[0].Teams[1].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
          
        }

        [TestMethod]
        public void TestCase8_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(8);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);  
            
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);

        }

        [TestMethod]
        public void TestCase9_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(9);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase10_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(10);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);

        }
        [TestMethod]
        public void TestCase11_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(11);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(3, result[0].Teams[1].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);

        }
        [TestMethod]
        public void TestCase12_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(12);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);

        }
        [TestMethod]
        public void TestCase13_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(13);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }

        [TestMethod]
        public void TestCase14_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(14);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(3, result[0].Teams[1].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);

        }
        [TestMethod]
        public void TestCase15_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(16);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }
        [TestMethod]
        public void TestCase16_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(16);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }
        [TestMethod]
        public void TestCase17_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(17);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);            

            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
            Assert.AreEqual(4, result[0].Teams[3].Id);
        }
        
        [TestMethod]
        public void TestCase18_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(18);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);

            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
            Assert.AreEqual(4, result[0].Teams[3].Id);
        }

        [TestMethod]
        public void TestCase19_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(19);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);

            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
            Assert.AreEqual(4, result[0].Teams[3].Id);
        }
        [TestMethod]
        public void TestCase20_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(20);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);
            Assert.AreEqual(4, result[1].Teams[1].Id);
        }

        [TestMethod]
        public void TestCase21_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(21);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);

            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
            Assert.AreEqual(4, result[0].Teams[3].Id);
        }
        [TestMethod]
        public void TestCase22_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(22);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);

            Assert.AreEqual(4, result[3].OrderNo);
            Assert.AreEqual(1, result[3].Teams.Count);
            Assert.AreEqual(4, result[3].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase23_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(23);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(3, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);
            Assert.AreEqual(4, result[1].Teams[2].Id);


        }

        [TestMethod]
        public void TestCase24_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(24);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(4, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase25_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(25);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(4, result[1].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase26_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(26);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(2, result[0].Teams[0].Id);
            Assert.AreEqual(4, result[0].Teams[1].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);
            
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(1, result[2].Teams[0].Id);
        }
        [TestMethod]
        public void TestCase27_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(27);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(4, result[0].Teams[0].Id);
            
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(3, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);           
            Assert.AreEqual(2, result[1].Teams[1].Id);
            Assert.AreEqual(3, result[1].Teams[2].Id);
        }

        [TestMethod]
        public void TestCase28_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(28);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);            

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(2, result[2].Teams.Count);
            Assert.AreEqual(4, result[2].Teams[0].Id);
            Assert.AreEqual(5, result[2].Teams[1].Id);
        }

        [TestMethod]
        public void TestCase29_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(29);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(3, result[0].Teams[1].Id);
            Assert.AreEqual(4, result[0].Teams[2].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase30_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(30);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(4, result[0].Teams[0].Id);
           
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(3, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);
            Assert.AreEqual(2, result[1].Teams[1].Id);
            Assert.AreEqual(3, result[1].Teams[2].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(5, result[2].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase31_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(31);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(4, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(3, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);
            Assert.AreEqual(5, result[1].Teams[2].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(2, result[2].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase32_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(32);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(5, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);
            Assert.AreEqual(4, result[1].Teams[1].Id);            

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(3, result[2].Teams[0].Id);

            Assert.AreEqual(4, result[3].OrderNo);
            Assert.AreEqual(1, result[3].Teams.Count);
            Assert.AreEqual(2, result[3].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase33_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(33);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(5, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(4, result[1].Teams[0].Id);
           
            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(2, result[2].Teams.Count);
            Assert.AreEqual(1, result[2].Teams[0].Id);
            Assert.AreEqual(3, result[2].Teams[1].Id);

            Assert.AreEqual(4, result[3].OrderNo);
            Assert.AreEqual(1, result[3].Teams.Count);
            Assert.AreEqual(2, result[3].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase34_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(34);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(6, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(3, result[1].Teams[0].Id);
            Assert.AreEqual(4, result[1].Teams[1].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(1, result[2].Teams[0].Id);            

            Assert.AreEqual(4, result[3].OrderNo);
            Assert.AreEqual(2, result[3].Teams.Count);
            Assert.AreEqual(2, result[3].Teams[0].Id);
            Assert.AreEqual(5, result[3].Teams[1].Id);

            Assert.AreEqual(5, result[4].OrderNo);
            Assert.AreEqual(1, result[4].Teams.Count);
            Assert.AreEqual(7, result[4].Teams[0].Id);
        }






    }
}