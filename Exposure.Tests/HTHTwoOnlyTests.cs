﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exposure.PoolStandingsProcessor.Model.HeadToHead;
using Exposure.PoolStandingsProcessor.Model;
using System.Collections.Generic;

namespace Exposure.Tests
{
    [TestClass]
    public class HTHTwoOnlyTests
    {
        private HTHNormal_DataCreator _dataCreator;
        private HeadToHeadComparisonProcessor _HTHComparison;

        [TestInitialize]
        public void Setup()
        {
            _dataCreator = new HTHNormal_DataCreator();
            _HTHComparison = new HeadToHeadComparisonProcessor();
        }

        [TestMethod]
        public void TestCase1_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(1);

            //act:
            var result = _HTHComparison.HeadToHeadTwoOnlyCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
        }

        [TestMethod]
        public void TestCase2_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(2);

            //act:
            var result = _HTHComparison.HeadToHeadTwoOnlyCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase3_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(3);

            //act:
            var result = _HTHComparison.HeadToHeadTwoOnlyCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
        }

        [TestMethod]
        public void TestCase4_34_ReturnsExpected()
        {
            for (int i = 4; i <= 34; i++) {
                //Arrange:
                var input = _dataCreator.CreateTestTeamsForHTHNormal(i);
                var expected_teamcount = input.Count;

                //act:
                var result = _HTHComparison.HeadToHeadTwoOnlyCompare(input);
                
                //assert:
                Assert.IsNotNull(result);
                Assert.AreEqual(1, result.Count);
                Assert.AreEqual(null, result[0].OrderNo);
                Assert.AreEqual(expected_teamcount, result[0].Teams.Count);                
            }
        }
    }
}
