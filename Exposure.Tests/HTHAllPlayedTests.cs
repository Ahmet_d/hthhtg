﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exposure.PoolStandingsProcessor.Model.HeadToHead;
using Exposure.PoolStandingsProcessor.Model;
using System.Collections.Generic;
using System.Linq;

namespace Exposure.Tests
{
    [TestClass]
    public class HTHAllPlayedTests
    {
        private HTHNormal_DataCreator _dataCreator;
        private HeadToHeadComparisonProcessor _HTHComparison;

        [TestInitialize]
        public void Setup()
        {
            _dataCreator = new HTHNormal_DataCreator();
            _HTHComparison = new HeadToHeadComparisonProcessor();
        }

        [TestMethod]
        public void TestCase2_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(2);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase3_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(3);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
        }

        [TestMethod]
        public void TestCase10_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(10);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(1, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(2, result[1].Teams.Count);
            Assert.AreEqual(2, result[1].Teams[0].Id);
            Assert.AreEqual(3, result[1].Teams[1].Id);

        }
        [TestMethod]
        public void TestCase16_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(16);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(3, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(2, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
        }
        [TestMethod]
        public void TestCase23_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(23);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);            

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);
            Assert.IsTrue(result[0].Teams.Select(x => x.Id).Except(new int[] { 1, 2, 3, 4 }).Count() == 0);            

        }
        [TestMethod]
        public void TestCase24_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(24);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);
            Assert.IsTrue(result[0].Teams.Select(x => x.Id).Except(new int[] { 1, 2, 3, 4 }).Count() == 0);

        }

        [TestMethod]
        public void TestCase38_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(38);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);
            Assert.AreEqual(1, result[0].Teams[0].Id);
            Assert.AreEqual(4, result[0].Teams[1].Id);
            Assert.AreEqual(3, result[0].Teams[2].Id);
            Assert.AreEqual(2, result[0].Teams[3].Id);
        }
    }
}
