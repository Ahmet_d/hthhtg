﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exposure.PoolStandingsProcessor.Model.HeadToHead;
using Exposure.PoolStandingsProcessor.Model;
using System.Collections.Generic;
using System.Linq;

namespace Exposure.Tests
{
    [TestClass]
    public class HTHCustomTests
    {
        private HTHNormal_DataCreator _dataCreator;
        private HeadToHeadComparisonProcessor _HTHComparison;

        [TestInitialize]
        public void Setup()
        {
            _dataCreator = new HTHNormal_DataCreator();
            _HTHComparison = new HeadToHeadComparisonProcessor();
        }

        [TestMethod]
        public void TestCaseCustom_FiveTeamsEightGames_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(35);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);

            Assert.AreEqual(1, result[0].OrderNo);
            Assert.AreEqual(2, result[0].Teams.Count);
            Assert.AreEqual(3, result[0].Teams[0].Id);
            Assert.AreEqual(4, result[0].Teams[1].Id);

            Assert.AreEqual(2, result[1].OrderNo);
            Assert.AreEqual(1, result[1].Teams.Count);
            Assert.AreEqual(1, result[1].Teams[0].Id);

            Assert.AreEqual(3, result[2].OrderNo);
            Assert.AreEqual(1, result[2].Teams.Count);
            Assert.AreEqual(2, result[2].Teams[0].Id);

            Assert.AreEqual(4, result[3].OrderNo);
            Assert.AreEqual(1, result[3].Teams.Count);
            Assert.AreEqual(5, result[3].Teams[0].Id);
        }

        [TestMethod]
        public void TestCase36_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(36);

            //act:
            var result = _HTHComparison.HeadToHeadAllPlayedCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(null, result[0].OrderNo);
            Assert.AreEqual(4, result[0].Teams.Count);
            Assert.IsTrue(result[0].Teams.Select(x => x.Id).Except(new int[] { 1, 2, 3, 4 }).Count() == 0);
        }

        [TestMethod]
        public void TestCase37_ReturnsExpected()
        {
            //Arrange:
            var input = _dataCreator.CreateTestTeamsForHTHNormal(37);

            //act:
            var result = _HTHComparison.HeadToHeadNormalCompare(input);

            //assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(2, result[3].Teams.Count);
            Assert.IsTrue(result[3].Teams.Select(x => x.Id).Except(new int[] { 1567677, 1567682 }).Count() == 0);
        }
    }
}
