﻿using System;
using System.Collections.Generic;
using System.Text;
using Exposure.PoolStandingsProcessor.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exposure.Tests
{
    [TestClass]
    public class HTHDiff_DataCreator
    {
        public List<TeamModel> CreateTestTeamsForHTHDiff(int testcase)
        {
            var result = new List<TeamModel>();

            #region Team definitions
            TeamModel team1 = new TeamModel() { Id = 1, Name = "Team1" };
            TeamModel team2 = new TeamModel() { Id = 2, Name = "Team2" };
            TeamModel team3 = new TeamModel() { Id = 3, Name = "Team3" };
            TeamModel team4 = new TeamModel() { Id = 4, Name = "Team4" };
            TeamModel team5 = new TeamModel() { Id = 5, Name = "Team5" };
            TeamModel team6 = new TeamModel() { Id = 6, Name = "Team6" };
            TeamModel team7 = new TeamModel() { Id = 7, Name = "Team7" };
            #endregion           

            switch (testcase)
            {
                #region Case 3 :  1: 1 win, +10 points; 2: 1 win, +5 points
                //expected result: 1:Team1,Team2
                case 3:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 5, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 5 });
                    result.Add(team1);
                    result.Add(team2);
                    break;
                #endregion
                #region Case 6 :  1: 1 win to 2 +10points; 2: 2 wins to 1 + 5 points; 2>3
                case 6:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 12, OpponentDivisionTeamPoolId = 1, OpponentScore = 8 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 8, OpponentDivisionTeamPoolId = 2, OpponentScore = 12 });
                    team2.Games.Add(new GameModel { Id = 4, TeamScore = 12, OpponentDivisionTeamPoolId = 1, OpponentScore = 11 });
                    team1.Games.Add(new GameModel { Id = 4, TeamScore = 11, OpponentDivisionTeamPoolId = 2, OpponentScore = 12 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 12, OpponentDivisionTeamPoolId = 2, OpponentScore = 23 });                    
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 12 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion


                #region Case 1 :  no matches 2 teams 
                //expected result: null: team1,team2
                case 1:
                    result.Add(team1);
                    result.Add(team2);
                    break;
                #endregion
                #region Case 2 :  1>2 
                //expected result: 1:Team1;2:Team2
                case 2:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    result.Add(team1);
                    result.Add(team2);
                    break;
                #endregion               
                #region Case 4 :  no matches 3 teams 
                case 4:
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 5 :  1>2 - three teams
                case 5:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion               
                #region Case 7 :  1>2,3>2 
                case 7:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 12, OpponentDivisionTeamPoolId = 3, OpponentScore = 23 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 2, OpponentScore = 12 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 8 :  1>2,1>3 
                case 8:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 25, OpponentDivisionTeamPoolId = 3, OpponentScore = 19 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 19, OpponentDivisionTeamPoolId = 1, OpponentScore = 25 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 9 :  1>2,2>3, 1>3  
                case 9:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 13, OpponentDivisionTeamPoolId = 1, OpponentScore = 23 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 13, OpponentDivisionTeamPoolId = 2, OpponentScore = 23 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 10 :  1>2,2=3, 1>3  
                case 10:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 33, OpponentDivisionTeamPoolId = 3, OpponentScore = 33 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 13, OpponentDivisionTeamPoolId = 1, OpponentScore = 23 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 33, OpponentDivisionTeamPoolId = 2, OpponentScore = 33 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 11 :  1>2,1=3, 3>2  
                case 11:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 2, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 1, OpponentDivisionTeamPoolId = 3, OpponentScore = 2 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 2 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 3, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 2, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 12 :  1>2,2=3  
                case 12:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 2, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 2 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 13 :  1=2,2=3  
                case 13:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 3, OpponentDivisionTeamPoolId = 2, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 3, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 14 :  1>2,1=3  
                case 14:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 2, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 2 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 15 :  1>2,2>3, 3>1  
                //expected result: 1:team1,team2,team3
                case 15:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 13, OpponentDivisionTeamPoolId = 3, OpponentScore = 23 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 23, OpponentDivisionTeamPoolId = 1, OpponentScore = 13 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 13, OpponentDivisionTeamPoolId = 2, OpponentScore = 23 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 16 :  1>2,2>3, 3>1  
                //expected result: 1:team1,team2,team3
                case 16:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 13, OpponentDivisionTeamPoolId = 2, OpponentScore = 23 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 13, OpponentDivisionTeamPoolId = 2, OpponentScore = 23 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 23, OpponentDivisionTeamPoolId = 1, OpponentScore = 13 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 13, OpponentDivisionTeamPoolId = 3, OpponentScore = 23 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    break;
                #endregion
                #region Case 17 :  no matches 4 teams 
                case 17:
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 18 :  1>2,3>4, 4 teams
                case 18:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 4, OpponentScore = 13 });
                    team4.Games.Add(new GameModel { Id = 2, TeamScore = 13, OpponentDivisionTeamPoolId = 3, OpponentScore = 23 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 19 :  1>2, 3>2, 3>4 4 teams
                case 19:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 30, OpponentDivisionTeamPoolId = 4, OpponentScore = 10 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 20 :  1=2,2>3,3=4 4 teams 
                case 20:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 4, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 10 });
                    team1.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 2, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 4, OpponentScore = 3 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 13, OpponentDivisionTeamPoolId = 2, OpponentScore = 23 });
                    team4.Games.Add(new GameModel { Id = 2, TeamScore = 3, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 21 : 1>2, 2>3, 3>4, 4>1 4 teams 
                case 21:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 3, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team1.Games.Add(new GameModel { Id = 4, TeamScore = 1, OpponentDivisionTeamPoolId = 4, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 1, OpponentDivisionTeamPoolId = 2, OpponentScore = 3 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 3, OpponentDivisionTeamPoolId = 4, OpponentScore = 1 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 1, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 5, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 22 : 1>2, 2>3, 3>4 4 teams 
                case 22:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 3, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 1, OpponentDivisionTeamPoolId = 2, OpponentScore = 3 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 3, OpponentDivisionTeamPoolId = 4, OpponentScore = 1 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 1, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 23 : 1>2, 1>3, 2>4 4 teams 
                //expected result: 1: team1,2:team2,team3,team4
                case 23:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 3, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 3, OpponentDivisionTeamPoolId = 4, OpponentScore = 1 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 1, OpponentDivisionTeamPoolId = 2, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 24 : 1>2, 1>3, 2>4,3>4 4 teams 
                case 24:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 3, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 23, OpponentDivisionTeamPoolId = 3, OpponentScore = 13 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 3, OpponentDivisionTeamPoolId = 4, OpponentScore = 1 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 1, OpponentDivisionTeamPoolId = 2, OpponentScore = 3 });
                    team3.Games.Add(new GameModel { Id = 4, TeamScore = 5, OpponentDivisionTeamPoolId = 4, OpponentScore = 3 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 1, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 25 : 1>2, 2>4,3>4 4 teams 
                ////expected result: 1: team1,team2,team3;2:team4
                case 25:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 3, OpponentDivisionTeamPoolId = 2, OpponentScore = 1 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 1, OpponentDivisionTeamPoolId = 1, OpponentScore = 3 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 3, OpponentDivisionTeamPoolId = 4, OpponentScore = 1 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 1, OpponentDivisionTeamPoolId = 2, OpponentScore = 3 });
                    team3.Games.Add(new GameModel { Id = 4, TeamScore = 5, OpponentDivisionTeamPoolId = 4, OpponentScore = 3 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 1, OpponentDivisionTeamPoolId = 3, OpponentScore = 3 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 26 : 1>3, 2>3,3>4 4 teams 
                case 26:
                    team4.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 10 });
                    team3.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 10 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 2, OpponentScore = 30 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 50, OpponentDivisionTeamPoolId = 1, OpponentScore = 30 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 30 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 27 : 1>2, 2>3,3>1,4>2 4 teams 
                //expected result: 1:team4;2:team1,team2,team3
                case 27:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 10 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 2, OpponentScore = 30 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 10 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 30 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 28 : 1>3, 2>3,3>4,3>5 5 teams 
                case 28:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 10 });
                    team3.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 10 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 2, OpponentScore = 30 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 50, OpponentDivisionTeamPoolId = 4, OpponentScore = 30 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 30 });
                    team3.Games.Add(new GameModel { Id = 4, TeamScore = 50, OpponentDivisionTeamPoolId = 5, OpponentScore = 30 });
                    team5.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 30 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    result.Add(team5);
                    break;
                #endregion
                #region Case 29 : 1>2, 1>3,3>4,4>1 4 teams  --FAIL: returns all as 1, cant determine the below one
                //expected result: 1:team1,team3,team4; 2:team2
                case 29:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 10 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 30 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 10 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 30 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 50, OpponentDivisionTeamPoolId = 4, OpponentScore = 30 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 30 });
                    team1.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 30 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 50, OpponentDivisionTeamPoolId = 1, OpponentScore = 30 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    break;
                #endregion
                #region Case 30 : 1=2, 2=3,4>2,2>5 5 teams 
                case 30:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 20, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 2, TeamScore = 20, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 20, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 3, TeamScore = 50, OpponentDivisionTeamPoolId = 2, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 30 });
                    team2.Games.Add(new GameModel { Id = 4, TeamScore = 50, OpponentDivisionTeamPoolId = 5, OpponentScore = 30 });
                    team5.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 2, OpponentScore = 30 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    result.Add(team5);
                    break;
                #endregion
                #region Case 31 : 1>2,5>1,3>2, 4>3, 4>5 5 teams 
                case 31:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    result.Add(team5);
                    break;
                #endregion
                #region Case 32 : 1>2,5>1,3>2, 4>3, 5>4, 1>3 5 teams 
                case 32:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    result.Add(team5);
                    break;
                #endregion
                #region Case 33 : 1>2,5>1,3>2, 4>3, 5>4, 4>1 5 teams 
                case 33:
                    team1.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team3);
                    result.Add(team4);
                    result.Add(team5);
                    break;
                #endregion
                #region Case 34 : 6>4, 6>3, 3>1, 4>1, 1>2, 1>5, 2>7, 5>7 7 teams 
                case 34:
                    team6.Games.Add(new GameModel { Id = 1, TeamScore = 30, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 1, TeamScore = 10, OpponentDivisionTeamPoolId = 6, OpponentScore = 20 });
                    team6.Games.Add(new GameModel { Id = 2, TeamScore = 30, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 2, TeamScore = 10, OpponentDivisionTeamPoolId = 6, OpponentScore = 20 });
                    team3.Games.Add(new GameModel { Id = 3, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 3, TeamScore = 10, OpponentDivisionTeamPoolId = 3, OpponentScore = 20 });
                    team4.Games.Add(new GameModel { Id = 4, TeamScore = 30, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 4, TeamScore = 10, OpponentDivisionTeamPoolId = 4, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team1.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 1, OpponentScore = 20 });
                    team2.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 7, OpponentScore = 20 });
                    team7.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 2, OpponentScore = 20 });
                    team5.Games.Add(new GameModel { Id = 5, TeamScore = 30, OpponentDivisionTeamPoolId = 7, OpponentScore = 20 });
                    team7.Games.Add(new GameModel { Id = 5, TeamScore = 10, OpponentDivisionTeamPoolId = 5, OpponentScore = 20 });
                    result.Add(team4);
                    result.Add(team5);
                    result.Add(team6);
                    result.Add(team1);
                    result.Add(team2);
                    result.Add(team7);
                    result.Add(team3);
                    break;
                #endregion
                default:
                    break;
            }

            return result;
        }
    }
}
